﻿using Gaminator.Enums;

namespace Gaminator
{
    public class SlotValues
    {
        public int SlotOne { get; set; }
        public int SlotTwo { get; set; }
        public int SlotThree { get; set; }

        public bool IsWiningCombination()
        {
            return (SlotOne == SlotTwo && SlotTwo == SlotThree) ||
                    (SlotOne + SlotTwo + SlotThree >= (int)WinCombinations.Sixteeen &&
                    SlotOne + SlotTwo + SlotThree <= (int)WinCombinations.Nineteen);
        }
    }
}