﻿namespace Gaminator
{
    partial class WrongBet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BetInfo = new System.Windows.Forms.Label();
            this.WrongBetCloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BetInfo
            // 
            this.BetInfo.AutoSize = true;
            this.BetInfo.Font = new System.Drawing.Font("Snap ITC", 12F);
            this.BetInfo.Location = new System.Drawing.Point(25, 22);
            this.BetInfo.Name = "BetInfo";
            this.BetInfo.Size = new System.Drawing.Size(60, 110);
            this.BetInfo.TabIndex = 0;
            this.BetInfo.Text = "label1\r\n\r\nssss\r\n\r\nssss";
            // 
            // WrongBetCloseButton
            // 
            this.WrongBetCloseButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.WrongBetCloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WrongBetCloseButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.WrongBetCloseButton.Location = new System.Drawing.Point(165, 12);
            this.WrongBetCloseButton.Name = "WrongBetCloseButton";
            this.WrongBetCloseButton.Size = new System.Drawing.Size(38, 40);
            this.WrongBetCloseButton.TabIndex = 21;
            this.WrongBetCloseButton.Text = "X";
            this.WrongBetCloseButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.WrongBetCloseButton.UseVisualStyleBackColor = true;
            this.WrongBetCloseButton.Click += new System.EventHandler(this.RulesCloseButton_Click);
            // 
            // WrongBet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(215, 161);
            this.Controls.Add(this.WrongBetCloseButton);
            this.Controls.Add(this.BetInfo);
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WrongBet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WrongBet";
            this.Load += new System.EventHandler(this.WrongBet_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label BetInfo;
        private System.Windows.Forms.Button WrongBetCloseButton;
    }
}