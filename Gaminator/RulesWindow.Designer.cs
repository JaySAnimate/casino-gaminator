﻿namespace Gaminator
{
    partial class RulesWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RulesWindow));
            this.RulesCloseButton = new System.Windows.Forms.Button();
            this.RulesText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // RulesCloseButton
            // 
            this.RulesCloseButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.RulesCloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RulesCloseButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.RulesCloseButton.Location = new System.Drawing.Point(387, 7);
            this.RulesCloseButton.Name = "RulesCloseButton";
            this.RulesCloseButton.Size = new System.Drawing.Size(38, 40);
            this.RulesCloseButton.TabIndex = 20;
            this.RulesCloseButton.Text = "X";
            this.RulesCloseButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.RulesCloseButton.UseVisualStyleBackColor = true;
            this.RulesCloseButton.Click += new System.EventHandler(this.RulesCloseButton_Click);
            // 
            // RulesText
            // 
            this.RulesText.AutoSize = true;
            this.RulesText.Font = new System.Drawing.Font("Snap ITC", 12F);
            this.RulesText.Location = new System.Drawing.Point(15, 55);
            this.RulesText.Name = "RulesText";
            this.RulesText.Size = new System.Drawing.Size(363, 440);
            this.RulesText.TabIndex = 21;
            this.RulesText.Text = resources.GetString("RulesText.Text");
            // 
            // RulesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(432, 509);
            this.Controls.Add(this.RulesText);
            this.Controls.Add(this.RulesCloseButton);
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RulesWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RulesWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button RulesCloseButton;
        private System.Windows.Forms.Label RulesText;
    }
}