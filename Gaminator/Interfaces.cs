﻿namespace Gaminator
{
    public interface ICasinoMachine
    {
        decimal Balance { get; set; }
        void CashIn(decimal amount = 100);
        void Spin();
        void Win();
    }
}