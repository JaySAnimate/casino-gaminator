﻿namespace Gaminator.Enums
{
    public enum WinCombinations
    {
        Zero,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,

        Sixteeen = 16,
        Seventeen = 17,
        Eightteen = 18,
        Nineteen = 19,

        Undefined = 1000000
    }
}
