﻿namespace Gaminator.Enums
{
    public enum Percentage
    {
        One = 20,
        Two = 35,
        Three = 50,
        Four = 70,
        Five = 85
    }
}
