﻿namespace Gaminator.Enums
{
    public enum Bets
    {
        None = 0,
        Min = 10,
        Mid = 15,
        Max = 20
    }
}
