﻿namespace Gaminator
{
    partial class GaminatorUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GaminatorUI));
            this.DisplayOne = new System.Windows.Forms.Label();
            this.DisplayThree = new System.Windows.Forms.Label();
            this.Balance = new System.Windows.Forms.Label();
            this.BalanceAmount = new System.Windows.Forms.Label();
            this.CurrentBet = new System.Windows.Forms.Label();
            this.CurrentBetAmount = new System.Windows.Forms.Label();
            this.SpinsAmount = new System.Windows.Forms.Label();
            this.Spins = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SetBalancePanel = new System.Windows.Forms.Panel();
            this.CashIn = new System.Windows.Forms.Button();
            this.DefaultHundred = new System.Windows.Forms.Button();
            this.CashInPanel = new System.Windows.Forms.Panel();
            this.CashInLabel = new System.Windows.Forms.Label();
            this.CashInTextBox = new System.Windows.Forms.TextBox();
            this.CashInButton = new System.Windows.Forms.Button();
            this.BetPanel = new System.Windows.Forms.Panel();
            this.label51015 = new System.Windows.Forms.Label();
            this.BetLabel = new System.Windows.Forms.Label();
            this.BetTextBox = new System.Windows.Forms.TextBox();
            this.BetButton = new System.Windows.Forms.Button();
            this.AwardPanel = new System.Windows.Forms.Panel();
            this.AgainButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.YouWinLabel = new System.Windows.Forms.Label();
            this.AwardBalanceLabel = new System.Windows.Forms.Label();
            this.AwardLabel = new System.Windows.Forms.Label();
            this.RulesButton = new System.Windows.Forms.Button();
            this.TrackBarRel = new System.Windows.Forms.TrackBar();
            this.DisplayTwo = new System.Windows.Forms.Label();
            this.SpinButton = new System.Windows.Forms.Button();
            this.BetAgainPanel = new System.Windows.Forms.Panel();
            this.BetAgainLabel101520 = new System.Windows.Forms.Label();
            this.BetAgainLabel = new System.Windows.Forms.Label();
            this.BetAgainTextBox = new System.Windows.Forms.TextBox();
            this.BetAgainBetButton = new System.Windows.Forms.Button();
            this.RelPanel = new System.Windows.Forms.Panel();
            this.LabelOne = new System.Windows.Forms.Label();
            this.LabelTwo = new System.Windows.Forms.Label();
            this.LabelThree = new System.Windows.Forms.Label();
            this.LabelFour = new System.Windows.Forms.Label();
            this.LabelFive = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NotEoughFundsLabel = new System.Windows.Forms.Label();
            this.BetNotEnoughFundsLabel = new System.Windows.Forms.Label();
            this.SetBalancePanel.SuspendLayout();
            this.CashInPanel.SuspendLayout();
            this.BetPanel.SuspendLayout();
            this.AwardPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarRel)).BeginInit();
            this.BetAgainPanel.SuspendLayout();
            this.RelPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DisplayOne
            // 
            this.DisplayOne.AutoSize = true;
            this.DisplayOne.Font = new System.Drawing.Font("Snap ITC", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisplayOne.Location = new System.Drawing.Point(125, 78);
            this.DisplayOne.Name = "DisplayOne";
            this.DisplayOne.Size = new System.Drawing.Size(193, 173);
            this.DisplayOne.TabIndex = 0;
            this.DisplayOne.Text = "█";
            // 
            // DisplayThree
            // 
            this.DisplayThree.AutoSize = true;
            this.DisplayThree.Font = new System.Drawing.Font("Snap ITC", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisplayThree.Location = new System.Drawing.Point(491, 78);
            this.DisplayThree.Name = "DisplayThree";
            this.DisplayThree.Size = new System.Drawing.Size(193, 173);
            this.DisplayThree.TabIndex = 2;
            this.DisplayThree.Text = "█";
            // 
            // Balance
            // 
            this.Balance.AutoSize = true;
            this.Balance.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.Balance.Location = new System.Drawing.Point(12, 9);
            this.Balance.Name = "Balance";
            this.Balance.Size = new System.Drawing.Size(141, 35);
            this.Balance.TabIndex = 4;
            this.Balance.Text = "Balance:";
            // 
            // BalanceAmount
            // 
            this.BalanceAmount.AutoSize = true;
            this.BalanceAmount.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.BalanceAmount.Location = new System.Drawing.Point(148, 9);
            this.BalanceAmount.Name = "BalanceAmount";
            this.BalanceAmount.Size = new System.Drawing.Size(37, 35);
            this.BalanceAmount.TabIndex = 5;
            this.BalanceAmount.Text = "$";
            // 
            // CurrentBet
            // 
            this.CurrentBet.AutoSize = true;
            this.CurrentBet.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.CurrentBet.Location = new System.Drawing.Point(490, 9);
            this.CurrentBet.Name = "CurrentBet";
            this.CurrentBet.Size = new System.Drawing.Size(215, 35);
            this.CurrentBet.TabIndex = 7;
            this.CurrentBet.Text = "Current Bet:";
            // 
            // CurrentBetAmount
            // 
            this.CurrentBetAmount.AutoSize = true;
            this.CurrentBetAmount.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.CurrentBetAmount.Location = new System.Drawing.Point(700, 9);
            this.CurrentBetAmount.Name = "CurrentBetAmount";
            this.CurrentBetAmount.Size = new System.Drawing.Size(37, 35);
            this.CurrentBetAmount.TabIndex = 6;
            this.CurrentBetAmount.Text = "$";
            // 
            // SpinsAmount
            // 
            this.SpinsAmount.AutoSize = true;
            this.SpinsAmount.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.SpinsAmount.Location = new System.Drawing.Point(403, 9);
            this.SpinsAmount.Name = "SpinsAmount";
            this.SpinsAmount.Size = new System.Drawing.Size(38, 35);
            this.SpinsAmount.TabIndex = 9;
            this.SpinsAmount.Text = "0";
            // 
            // Spins
            // 
            this.Spins.AutoSize = true;
            this.Spins.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.Spins.Location = new System.Drawing.Point(286, 9);
            this.Spins.Name = "Spins";
            this.Spins.Size = new System.Drawing.Size(111, 35);
            this.Spins.TabIndex = 8;
            this.Spins.Text = "Spins:";
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.button2.Location = new System.Drawing.Point(5, 515);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 40);
            this.button2.TabIndex = 10;
            this.button2.Text = "Leave Room";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.LeaveRoom_Click);
            // 
            // SetBalancePanel
            // 
            this.SetBalancePanel.Controls.Add(this.CashIn);
            this.SetBalancePanel.Controls.Add(this.DefaultHundred);
            this.SetBalancePanel.Location = new System.Drawing.Point(225, 337);
            this.SetBalancePanel.Name = "SetBalancePanel";
            this.SetBalancePanel.Size = new System.Drawing.Size(318, 176);
            this.SetBalancePanel.TabIndex = 12;
            // 
            // CashIn
            // 
            this.CashIn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.CashIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CashIn.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.CashIn.Location = new System.Drawing.Point(35, 90);
            this.CashIn.Name = "CashIn";
            this.CashIn.Size = new System.Drawing.Size(251, 48);
            this.CashIn.TabIndex = 14;
            this.CashIn.Text = "Cash In";
            this.CashIn.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CashIn.UseVisualStyleBackColor = true;
            this.CashIn.Click += new System.EventHandler(this.CashIn_Click);
            // 
            // DefaultHundred
            // 
            this.DefaultHundred.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.DefaultHundred.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DefaultHundred.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.DefaultHundred.Location = new System.Drawing.Point(35, 22);
            this.DefaultHundred.Name = "DefaultHundred";
            this.DefaultHundred.Size = new System.Drawing.Size(251, 49);
            this.DefaultHundred.TabIndex = 13;
            this.DefaultHundred.Text = "$100";
            this.DefaultHundred.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.DefaultHundred.UseVisualStyleBackColor = true;
            this.DefaultHundred.Click += new System.EventHandler(this.DefaultHundred_Click);
            // 
            // CashInPanel
            // 
            this.CashInPanel.Controls.Add(this.CashInLabel);
            this.CashInPanel.Controls.Add(this.CashInTextBox);
            this.CashInPanel.Controls.Add(this.CashInButton);
            this.CashInPanel.Location = new System.Drawing.Point(5, 302);
            this.CashInPanel.Name = "CashInPanel";
            this.CashInPanel.Size = new System.Drawing.Size(318, 176);
            this.CashInPanel.TabIndex = 15;
            // 
            // CashInLabel
            // 
            this.CashInLabel.AutoSize = true;
            this.CashInLabel.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.CashInLabel.Location = new System.Drawing.Point(49, 30);
            this.CashInLabel.Name = "CashInLabel";
            this.CashInLabel.Size = new System.Drawing.Size(228, 27);
            this.CashInLabel.TabIndex = 16;
            this.CashInLabel.Text = "Enter The Amount:";
            // 
            // CashInTextBox
            // 
            this.CashInTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.CashInTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CashInTextBox.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.CashInTextBox.Location = new System.Drawing.Point(82, 60);
            this.CashInTextBox.Name = "CashInTextBox";
            this.CashInTextBox.Size = new System.Drawing.Size(158, 42);
            this.CashInTextBox.TabIndex = 15;
            this.CashInTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CashInButton
            // 
            this.CashInButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.CashInButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CashInButton.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.CashInButton.Location = new System.Drawing.Point(35, 110);
            this.CashInButton.Name = "CashInButton";
            this.CashInButton.Size = new System.Drawing.Size(251, 48);
            this.CashInButton.TabIndex = 14;
            this.CashInButton.Text = "Cash In";
            this.CashInButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CashInButton.UseVisualStyleBackColor = true;
            this.CashInButton.Click += new System.EventHandler(this.CashInButton_Click);
            // 
            // BetPanel
            // 
            this.BetPanel.Controls.Add(this.BetNotEnoughFundsLabel);
            this.BetPanel.Controls.Add(this.label51015);
            this.BetPanel.Controls.Add(this.BetLabel);
            this.BetPanel.Controls.Add(this.BetTextBox);
            this.BetPanel.Controls.Add(this.BetButton);
            this.BetPanel.Location = new System.Drawing.Point(474, 299);
            this.BetPanel.Name = "BetPanel";
            this.BetPanel.Size = new System.Drawing.Size(318, 176);
            this.BetPanel.TabIndex = 17;
            // 
            // label51015
            // 
            this.label51015.AutoSize = true;
            this.label51015.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.label51015.Location = new System.Drawing.Point(81, 3);
            this.label51015.Name = "label51015";
            this.label51015.Size = new System.Drawing.Size(159, 27);
            this.label51015.TabIndex = 17;
            this.label51015.Text = "10 - 15  - 20";
            this.label51015.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BetLabel
            // 
            this.BetLabel.AutoSize = true;
            this.BetLabel.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.BetLabel.Location = new System.Drawing.Point(130, 30);
            this.BetLabel.Name = "BetLabel";
            this.BetLabel.Size = new System.Drawing.Size(61, 27);
            this.BetLabel.TabIndex = 16;
            this.BetLabel.Text = "Bet:";
            this.BetLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BetTextBox
            // 
            this.BetTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.BetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BetTextBox.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.BetTextBox.Location = new System.Drawing.Point(82, 60);
            this.BetTextBox.Name = "BetTextBox";
            this.BetTextBox.Size = new System.Drawing.Size(158, 42);
            this.BetTextBox.TabIndex = 15;
            this.BetTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BetButton
            // 
            this.BetButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.BetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BetButton.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.BetButton.Location = new System.Drawing.Point(35, 110);
            this.BetButton.Name = "BetButton";
            this.BetButton.Size = new System.Drawing.Size(251, 48);
            this.BetButton.TabIndex = 14;
            this.BetButton.Text = "Bet";
            this.BetButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BetButton.UseVisualStyleBackColor = true;
            this.BetButton.Click += new System.EventHandler(this.BetButton_Click);
            // 
            // AwardPanel
            // 
            this.AwardPanel.Controls.Add(this.AgainButton);
            this.AwardPanel.Controls.Add(this.ExitButton);
            this.AwardPanel.Controls.Add(this.YouWinLabel);
            this.AwardPanel.Controls.Add(this.AwardBalanceLabel);
            this.AwardPanel.Controls.Add(this.AwardLabel);
            this.AwardPanel.Location = new System.Drawing.Point(78, 263);
            this.AwardPanel.Name = "AwardPanel";
            this.AwardPanel.Size = new System.Drawing.Size(646, 176);
            this.AwardPanel.TabIndex = 15;
            // 
            // AgainButton
            // 
            this.AgainButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.AgainButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AgainButton.Font = new System.Drawing.Font("Snap ITC", 25F);
            this.AgainButton.Location = new System.Drawing.Point(496, 61);
            this.AgainButton.Name = "AgainButton";
            this.AgainButton.Size = new System.Drawing.Size(147, 56);
            this.AgainButton.TabIndex = 22;
            this.AgainButton.Text = "Again";
            this.AgainButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AgainButton.UseVisualStyleBackColor = true;
            this.AgainButton.Click += new System.EventHandler(this.AgainButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.Font = new System.Drawing.Font("Snap ITC", 25F);
            this.ExitButton.Location = new System.Drawing.Point(3, 61);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(121, 56);
            this.ExitButton.TabIndex = 18;
            this.ExitButton.Text = "Exit";
            this.ExitButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // YouWinLabel
            // 
            this.YouWinLabel.AutoSize = true;
            this.YouWinLabel.Font = new System.Drawing.Font("Snap ITC", 25F);
            this.YouWinLabel.Location = new System.Drawing.Point(193, 11);
            this.YouWinLabel.Name = "YouWinLabel";
            this.YouWinLabel.Size = new System.Drawing.Size(240, 44);
            this.YouWinLabel.TabIndex = 21;
            this.YouWinLabel.Text = "You Win!!!";
            // 
            // AwardBalanceLabel
            // 
            this.AwardBalanceLabel.AutoSize = true;
            this.AwardBalanceLabel.Font = new System.Drawing.Font("Snap ITC", 50F);
            this.AwardBalanceLabel.Location = new System.Drawing.Point(199, 60);
            this.AwardBalanceLabel.Name = "AwardBalanceLabel";
            this.AwardBalanceLabel.Size = new System.Drawing.Size(202, 86);
            this.AwardBalanceLabel.TabIndex = 20;
            this.AwardBalanceLabel.Text = "$90";
            // 
            // AwardLabel
            // 
            this.AwardLabel.AutoSize = true;
            this.AwardLabel.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.AwardLabel.Location = new System.Drawing.Point(264, 146);
            this.AwardLabel.Name = "AwardLabel";
            this.AwardLabel.Size = new System.Drawing.Size(86, 27);
            this.AwardLabel.TabIndex = 19;
            this.AwardLabel.Text = "Award";
            // 
            // RulesButton
            // 
            this.RulesButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.RulesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RulesButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.RulesButton.Location = new System.Drawing.Point(679, 515);
            this.RulesButton.Name = "RulesButton";
            this.RulesButton.Size = new System.Drawing.Size(93, 40);
            this.RulesButton.TabIndex = 19;
            this.RulesButton.Text = "Rules";
            this.RulesButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.RulesButton.UseVisualStyleBackColor = true;
            this.RulesButton.Click += new System.EventHandler(this.RulesButton_Click);
            // 
            // TrackBarRel
            // 
            this.TrackBarRel.Location = new System.Drawing.Point(10, 11);
            this.TrackBarRel.Maximum = 5;
            this.TrackBarRel.Minimum = 1;
            this.TrackBarRel.Name = "TrackBarRel";
            this.TrackBarRel.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.TrackBarRel.Size = new System.Drawing.Size(45, 227);
            this.TrackBarRel.TabIndex = 20;
            this.TrackBarRel.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.TrackBarRel.Value = 3;
            // 
            // DisplayTwo
            // 
            this.DisplayTwo.AutoSize = true;
            this.DisplayTwo.Font = new System.Drawing.Font("Snap ITC", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisplayTwo.Location = new System.Drawing.Point(308, 78);
            this.DisplayTwo.Name = "DisplayTwo";
            this.DisplayTwo.Size = new System.Drawing.Size(193, 173);
            this.DisplayTwo.TabIndex = 1;
            this.DisplayTwo.Text = "█";
            // 
            // SpinButton
            // 
            this.SpinButton.Enabled = false;
            this.SpinButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.SpinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SpinButton.Font = new System.Drawing.Font("Snap ITC", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpinButton.Location = new System.Drawing.Point(225, 263);
            this.SpinButton.Name = "SpinButton";
            this.SpinButton.Size = new System.Drawing.Size(318, 68);
            this.SpinButton.TabIndex = 3;
            this.SpinButton.Text = "Spin";
            this.SpinButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SpinButton.UseVisualStyleBackColor = true;
            this.SpinButton.Click += new System.EventHandler(this.Spin_Click);
            // 
            // BetAgainPanel
            // 
            this.BetAgainPanel.Controls.Add(this.NotEoughFundsLabel);
            this.BetAgainPanel.Controls.Add(this.BetAgainLabel101520);
            this.BetAgainPanel.Controls.Add(this.BetAgainLabel);
            this.BetAgainPanel.Controls.Add(this.BetAgainTextBox);
            this.BetAgainPanel.Controls.Add(this.BetAgainBetButton);
            this.BetAgainPanel.Location = new System.Drawing.Point(225, 95);
            this.BetAgainPanel.Name = "BetAgainPanel";
            this.BetAgainPanel.Size = new System.Drawing.Size(318, 176);
            this.BetAgainPanel.TabIndex = 18;
            // 
            // BetAgainLabel101520
            // 
            this.BetAgainLabel101520.AutoSize = true;
            this.BetAgainLabel101520.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.BetAgainLabel101520.Location = new System.Drawing.Point(86, 3);
            this.BetAgainLabel101520.Name = "BetAgainLabel101520";
            this.BetAgainLabel101520.Size = new System.Drawing.Size(150, 27);
            this.BetAgainLabel101520.TabIndex = 17;
            this.BetAgainLabel101520.Text = "10 - 15 - 20";
            this.BetAgainLabel101520.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BetAgainLabel
            // 
            this.BetAgainLabel.AutoSize = true;
            this.BetAgainLabel.Font = new System.Drawing.Font("Snap ITC", 12.5F);
            this.BetAgainLabel.Location = new System.Drawing.Point(3, 30);
            this.BetAgainLabel.Name = "BetAgainLabel";
            this.BetAgainLabel.Size = new System.Drawing.Size(313, 23);
            this.BetAgainLabel.TabIndex = 16;
            this.BetAgainLabel.Text = "You Have No Spins, Bet Again:";
            this.BetAgainLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BetAgainTextBox
            // 
            this.BetAgainTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.BetAgainTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BetAgainTextBox.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.BetAgainTextBox.Location = new System.Drawing.Point(82, 60);
            this.BetAgainTextBox.Name = "BetAgainTextBox";
            this.BetAgainTextBox.Size = new System.Drawing.Size(158, 42);
            this.BetAgainTextBox.TabIndex = 15;
            this.BetAgainTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BetAgainBetButton
            // 
            this.BetAgainBetButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.BetAgainBetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BetAgainBetButton.Font = new System.Drawing.Font("Snap ITC", 20F);
            this.BetAgainBetButton.Location = new System.Drawing.Point(35, 110);
            this.BetAgainBetButton.Name = "BetAgainBetButton";
            this.BetAgainBetButton.Size = new System.Drawing.Size(251, 48);
            this.BetAgainBetButton.TabIndex = 14;
            this.BetAgainBetButton.Text = "Bet";
            this.BetAgainBetButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BetAgainBetButton.UseVisualStyleBackColor = true;
            this.BetAgainBetButton.Click += new System.EventHandler(this.BetAgainBetButton_Click);
            // 
            // RelPanel
            // 
            this.RelPanel.Controls.Add(this.label1);
            this.RelPanel.Controls.Add(this.LabelOne);
            this.RelPanel.Controls.Add(this.LabelTwo);
            this.RelPanel.Controls.Add(this.LabelThree);
            this.RelPanel.Controls.Add(this.LabelFour);
            this.RelPanel.Controls.Add(this.LabelFive);
            this.RelPanel.Controls.Add(this.TrackBarRel);
            this.RelPanel.Location = new System.Drawing.Point(9, 50);
            this.RelPanel.Name = "RelPanel";
            this.RelPanel.Size = new System.Drawing.Size(134, 268);
            this.RelPanel.TabIndex = 21;
            // 
            // LabelOne
            // 
            this.LabelOne.AutoSize = true;
            this.LabelOne.Font = new System.Drawing.Font("Snap ITC", 8F);
            this.LabelOne.Location = new System.Drawing.Point(54, 217);
            this.LabelOne.Name = "LabelOne";
            this.LabelOne.Size = new System.Drawing.Size(30, 15);
            this.LabelOne.TabIndex = 26;
            this.LabelOne.Text = "One";
            // 
            // LabelTwo
            // 
            this.LabelTwo.AutoSize = true;
            this.LabelTwo.Font = new System.Drawing.Font("Snap ITC", 8F);
            this.LabelTwo.Location = new System.Drawing.Point(52, 167);
            this.LabelTwo.Name = "LabelTwo";
            this.LabelTwo.Size = new System.Drawing.Size(33, 15);
            this.LabelTwo.TabIndex = 25;
            this.LabelTwo.Text = "Two";
            // 
            // LabelThree
            // 
            this.LabelThree.AutoSize = true;
            this.LabelThree.Font = new System.Drawing.Font("Snap ITC", 8F);
            this.LabelThree.Location = new System.Drawing.Point(53, 117);
            this.LabelThree.Name = "LabelThree";
            this.LabelThree.Size = new System.Drawing.Size(42, 15);
            this.LabelThree.TabIndex = 24;
            this.LabelThree.Text = "Three";
            // 
            // LabelFour
            // 
            this.LabelFour.AutoSize = true;
            this.LabelFour.Font = new System.Drawing.Font("Snap ITC", 8F);
            this.LabelFour.Location = new System.Drawing.Point(52, 66);
            this.LabelFour.Name = "LabelFour";
            this.LabelFour.Size = new System.Drawing.Size(35, 15);
            this.LabelFour.TabIndex = 23;
            this.LabelFour.Text = "Four";
            // 
            // LabelFive
            // 
            this.LabelFive.AutoSize = true;
            this.LabelFive.Font = new System.Drawing.Font("Snap ITC", 8F);
            this.LabelFive.Location = new System.Drawing.Point(53, 16);
            this.LabelFive.Name = "LabelFive";
            this.LabelFive.Size = new System.Drawing.Size(35, 15);
            this.LabelFive.TabIndex = 22;
            this.LabelFive.Text = "Five";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Snap ITC", 8F);
            this.label1.Location = new System.Drawing.Point(33, 243);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 27;
            this.label1.Text = "Probability";
            // 
            // NotEoughFundsLabel
            // 
            this.NotEoughFundsLabel.AutoSize = true;
            this.NotEoughFundsLabel.Font = new System.Drawing.Font("Snap ITC", 12.5F);
            this.NotEoughFundsLabel.Location = new System.Drawing.Point(62, 149);
            this.NotEoughFundsLabel.Name = "NotEoughFundsLabel";
            this.NotEoughFundsLabel.Size = new System.Drawing.Size(201, 23);
            this.NotEoughFundsLabel.TabIndex = 18;
            this.NotEoughFundsLabel.Text = "Not Enough Funds !";
            this.NotEoughFundsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BetNotEnoughFundsLabel
            // 
            this.BetNotEnoughFundsLabel.AutoSize = true;
            this.BetNotEnoughFundsLabel.Font = new System.Drawing.Font("Snap ITC", 12.5F);
            this.BetNotEnoughFundsLabel.Location = new System.Drawing.Point(59, 148);
            this.BetNotEnoughFundsLabel.Name = "BetNotEnoughFundsLabel";
            this.BetNotEnoughFundsLabel.Size = new System.Drawing.Size(201, 23);
            this.BetNotEnoughFundsLabel.TabIndex = 19;
            this.BetNotEnoughFundsLabel.Text = "Not Enough Funds !";
            this.BetNotEnoughFundsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GaminatorUI
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.RelPanel);
            this.Controls.Add(this.RulesButton);
            this.Controls.Add(this.AwardPanel);
            this.Controls.Add(this.BetAgainPanel);
            this.Controls.Add(this.SetBalancePanel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.SpinsAmount);
            this.Controls.Add(this.Spins);
            this.Controls.Add(this.CurrentBet);
            this.Controls.Add(this.CurrentBetAmount);
            this.Controls.Add(this.BalanceAmount);
            this.Controls.Add(this.Balance);
            this.Controls.Add(this.SpinButton);
            this.Controls.Add(this.DisplayThree);
            this.Controls.Add(this.DisplayTwo);
            this.Controls.Add(this.DisplayOne);
            this.Controls.Add(this.BetPanel);
            this.Controls.Add(this.CashInPanel);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GaminatorUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.SetBalancePanel.ResumeLayout(false);
            this.CashInPanel.ResumeLayout(false);
            this.CashInPanel.PerformLayout();
            this.BetPanel.ResumeLayout(false);
            this.BetPanel.PerformLayout();
            this.AwardPanel.ResumeLayout(false);
            this.AwardPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarRel)).EndInit();
            this.BetAgainPanel.ResumeLayout(false);
            this.BetAgainPanel.PerformLayout();
            this.RelPanel.ResumeLayout(false);
            this.RelPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DisplayOne;
        private System.Windows.Forms.Label DisplayThree;
        private System.Windows.Forms.Label Balance;
        private System.Windows.Forms.Label BalanceAmount;
        private System.Windows.Forms.Label CurrentBet;
        private System.Windows.Forms.Label CurrentBetAmount;
        private System.Windows.Forms.Label SpinsAmount;
        private System.Windows.Forms.Label Spins;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel SetBalancePanel;
        private System.Windows.Forms.Button CashIn;
        private System.Windows.Forms.Button DefaultHundred;
        private System.Windows.Forms.Panel CashInPanel;
        private System.Windows.Forms.Label CashInLabel;
        private System.Windows.Forms.TextBox CashInTextBox;
        private System.Windows.Forms.Button CashInButton;
        private System.Windows.Forms.Panel BetPanel;
        private System.Windows.Forms.Label label51015;
        private System.Windows.Forms.Label BetLabel;
        private System.Windows.Forms.TextBox BetTextBox;
        private System.Windows.Forms.Button BetButton;
        private System.Windows.Forms.Panel AwardPanel;
        private System.Windows.Forms.Label AwardBalanceLabel;
        private System.Windows.Forms.Label AwardLabel;
        private System.Windows.Forms.Label YouWinLabel;
        private System.Windows.Forms.Button AgainButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button RulesButton;
        private System.Windows.Forms.TrackBar TrackBarRel;
        private System.Windows.Forms.Label DisplayTwo;
        private System.Windows.Forms.Button SpinButton;
        private System.Windows.Forms.Panel BetAgainPanel;
        private System.Windows.Forms.Label BetAgainLabel101520;
        private System.Windows.Forms.Label BetAgainLabel;
        private System.Windows.Forms.TextBox BetAgainTextBox;
        private System.Windows.Forms.Button BetAgainBetButton;
        private System.Windows.Forms.Panel RelPanel;
        private System.Windows.Forms.Label LabelOne;
        private System.Windows.Forms.Label LabelTwo;
        private System.Windows.Forms.Label LabelThree;
        private System.Windows.Forms.Label LabelFour;
        private System.Windows.Forms.Label LabelFive;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label NotEoughFundsLabel;
        private System.Windows.Forms.Label BetNotEnoughFundsLabel;
    }
}

