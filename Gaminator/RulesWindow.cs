﻿using System;
using System.Windows.Forms;

namespace Gaminator
{
    public partial class RulesWindow : Form
    {
        public RulesWindow()
        {
            InitializeComponent();
        }

        private void RulesCloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
