﻿using Gaminator.Enums;
using System;

namespace Gaminator
{
    class Gaminator : ICasinoMachine
    {
        private decimal _balance;

        #region Properties
        public WinMethods WinMethod { get; private set; }

        public WinCombinations WinCombination { get; private set; }

        private Bets CurrentBet { set; get; }

        private int StepsToWin { get; set; }

        private int CurrentStep { set; get; }

        public bool Won { get; set; }

        public int Spins { get; private set; }

        public decimal Balance
        {
            set
            {
                if (value > 0)
                    _balance = value;
                else if (value == 0)
                    _balance = 0;

                else
                    throw new Exception("Balance must be Positive.");
            }
            get => _balance;
        }
        #endregion
        
        public Gaminator()
        {
            Balance = 100;
            Spins = 5;
        }

        public void CashIn(decimal amount = 100)
        {            
            // I suggest you to play with $100 in your balance. Do you agree ?
            // if yes -> 100
            // else -> Ok, Cash in please
            Balance = amount;
        }

        // Հաշվարկվում է համապատասխան քայլերի քանակը
        // նախընտրելի հավանականությամբ խաղը սկսելու համար
        public void DetermineStepsToWin(int probability)
        {
            // Determines the steps in order to win according to 
            // the maximum amount of bets they can do,
            // that is Balance / minBetAmountAvailable
            int ConcretStepCount = 0;
            switch (probability)
            {
                case 1:
                    ConcretStepCount = CalculateSteps(Percentage.Five);
                    break;
                case 2:
                    ConcretStepCount = CalculateSteps(Percentage.Four);
                    break;
                case 3:
                    ConcretStepCount = CalculateSteps(Percentage.Three);
                    break;
                case 4:
                    ConcretStepCount = CalculateSteps(Percentage.Two);
                    break;
                case 5:
                    ConcretStepCount = CalculateSteps(Percentage.One);
                    break;
            }
            
            int LeftBound = 2 * ConcretStepCount / 3;
            int RightBound = ConcretStepCount;

            StepsToWin = new Random().Next(LeftBound, RightBound);
        }

        // Բանաձև որով հաշվարկվում է քայլերի քանակը ըստ հավանակաության
        // (օգտագործված է նախորդ ֆունկցիայում)
        private int CalculateSteps(Percentage percentage)
        {
            decimal Up = (decimal)percentage * Balance;
            decimal Down = 20m * (decimal)Bets.Mid;
            return (int)(Up / Down);
        }

        public void DetermineWinMethod()
        {
            // there are two methods to win
            int methodByNumber = new Random().Next(1, 3);
            WinMethod = (WinMethods)methodByNumber;
        }

        public WinCombinations DetermineWinningCombination()
        {
            if (WinMethod == WinMethods.Identity)
            {
                int combinationNumber = new Random().Next(0, 10);
                return (WinCombinations)combinationNumber;
            }
            else if (WinMethod == WinMethods.Sum)
            {
                int combinationNumber = new Random().Next(16, 20);
                return (WinCombinations)combinationNumber;
            }
            else
                return WinCombinations.Undefined;
        }

        public SlotValues DetermineSlotValues()
        {
            SlotValues Values = new SlotValues();
            if (Won == false)
            {
                // Show some random values but be careful in order 
                // not to generate a wining condition accidentally 
                // as on this stage the game is not won yet.
                do
                {
                    Values.SlotOne = new Random().Next(0, 10);
                    Values.SlotTwo = new Random().Next(0, 10);
                    Values.SlotThree = new Random().Next(0, 10);
                } while (Values.IsWiningCombination());
            }
            else
            {
                if (WinMethod == WinMethods.Sum)
                {
                    int wincombination = (int)WinCombination;
                    Values.SlotOne = new Random().Next(0, 10); ;
                    Values.SlotTwo = new Random().Next(0, wincombination - Values.SlotOne + 1);
                    Values.SlotThree = wincombination - Values.SlotOne - Values.SlotTwo;
                }
                else if (WinMethod == WinMethods.Identity)
                    Values.SlotOne = Values.SlotTwo = Values.SlotThree = (int)WinCombination;
            }
            return Values;
        }

        public void Bet(Bets bet)
        {
            try
            {
                CurrentBet = bet;
                Balance -= (decimal)CurrentBet;

                if (WinCombination != WinCombinations.Six)
                    Spins = 5;

                WinCombination = WinCombinations.Undefined;
            }
            catch
            {
                throw;
            }
        }

        public void Spin()
        {
            if (Spins != 0)
            {
                Won = false;
                Spins--;
                if (++CurrentStep == StepsToWin)
                {
                    Won = true;
                    CurrentStep = 0;
                    WinCombination = DetermineWinningCombination();
                }
            }
        }

        public void CalculateIdentityAward()
        {
            decimal award;
            switch (WinCombination)
            {
                case WinCombinations.Zero: // Return Money
                    Balance += (decimal)CurrentBet;
                    break;
                case WinCombinations.One: // +10%
                    award = (decimal)CurrentBet + (decimal)CurrentBet * 0.1m;
                    Balance += award;
                    break;
                case WinCombinations.Two: // +20%
                    award = (decimal)CurrentBet + (decimal)CurrentBet * 0.2m;
                    Balance += award;
                    break;
                case WinCombinations.Three: // +30%
                    award = (decimal)CurrentBet + (decimal)CurrentBet * 0.3m;
                    Balance += award;
                    break;
                case WinCombinations.Four: //+40%
                    award = (decimal)CurrentBet + (decimal)CurrentBet * 0.4m;
                    Balance += award;
                    break;
                case WinCombinations.Five: // x2
                    Balance += 2 * (decimal)CurrentBet;
                    break;
                case WinCombinations.Six: // +4 Spins
                    Spins += 4;
                    Balance += (decimal)CurrentBet;
                    CurrentBet = Bets.None;
                    break;
                case WinCombinations.Seven: // x5
                    Balance += 5 * (decimal)CurrentBet;
                    break;
                case WinCombinations.Eight: // +50%
                    award = (decimal)CurrentBet + (decimal)CurrentBet / 2m;
                    Balance += award;
                    break;
                case WinCombinations.Nine: // +90%
                    award = (decimal)CurrentBet + (decimal)CurrentBet * 0.9m;
                    Balance += award;
                    break;
            }
        }

        public void CalculateSumAward()
        {
            decimal award;
            switch (WinCombination)
            {
                case WinCombinations.Sixteeen: // Return Money
                    Balance += (decimal)CurrentBet;
                    break;
                case WinCombinations.Seventeen: // +60%
                    award = (decimal)CurrentBet + (decimal)CurrentBet * 0.6m;
                    Balance += award;
                    break;
                case WinCombinations.Eightteen: // +70%
                    award = (decimal)CurrentBet + (decimal)CurrentBet * 0.7m;
                    Balance += award;
                    break;
                case WinCombinations.Nineteen: // +80%
                    award = (decimal)CurrentBet + (decimal)CurrentBet * 0.8m;
                    Balance += award;
                    break;
            }
        }

        public void Win()
        {
            switch (WinMethod)
            {
                case WinMethods.Identity:
                    CalculateIdentityAward();
                    break;
                case WinMethods.Sum:
                    CalculateSumAward();
                    break;
            }
        }
    }
}