﻿namespace Gaminator
{
    partial class WrongCash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.WronCashCloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Snap ITC", 12F);
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(506, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Entered cash must be greather than or equal to $100.";
            // 
            // WronCashCloseButton
            // 
            this.WronCashCloseButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.WronCashCloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WronCashCloseButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.WronCashCloseButton.Location = new System.Drawing.Point(524, 3);
            this.WronCashCloseButton.Name = "WronCashCloseButton";
            this.WronCashCloseButton.Size = new System.Drawing.Size(38, 40);
            this.WronCashCloseButton.TabIndex = 22;
            this.WronCashCloseButton.Text = "X";
            this.WronCashCloseButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.WronCashCloseButton.UseVisualStyleBackColor = true;
            this.WronCashCloseButton.Click += new System.EventHandler(this.WronCashCloseButton_Click);
            // 
            // WrongCash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(565, 77);
            this.Controls.Add(this.WronCashCloseButton);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "WrongCash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WrongCash";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button WronCashCloseButton;
    }
}