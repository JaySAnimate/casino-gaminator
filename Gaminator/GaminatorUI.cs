﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Gaminator.Enums;

namespace Gaminator
{
    public partial class GaminatorUI : Form
    {
        private readonly Gaminator _game;
        private readonly RulesWindow _rulePopup;
        private readonly WrongBet _wrongBet;
        private readonly WrongCash _wrongCash;

        public GaminatorUI()
        {
            InitializeComponent();

            _game = new Gaminator();
            _rulePopup = new RulesWindow();
            _wrongBet = new WrongBet();
            _wrongCash = new WrongCash();

            InitializeProbabilityBar();

            NotEoughFundsLabel.Hide();
            BetNotEnoughFundsLabel.Hide();
            CashInPanel.Hide();
            BetPanel.Hide();
            BetAgainPanel.Hide();
            AwardPanel.Hide();
        }

        private void SetDisplays(SlotValues values)
        {
            DisplayOne.Text = values.SlotOne.ToString();
            DisplayTwo.Text = values.SlotTwo.ToString();
            DisplayThree.Text = values.SlotThree.ToString();
        }

        private void ShowPanel(Panel panel)
        {
            panel.Location = new Point(225, 337);
            panel.Show();
        }

        private void InitializeProbabilityBar()
        {
            LabelOne.Text = string.Format("{0}%", (int)Percentage.One);
            LabelTwo.Text = string.Format("{0}%", (int)Percentage.Two);
            LabelThree.Text = string.Format("{0}%", (int)Percentage.Three);
            LabelFour.Text = string.Format("{0}%", (int)Percentage.Four);
            LabelFive.Text = string.Format("{0}%", (int)Percentage.Five);
        }


        #region Button Events
        ///////////////////////////////////////////////////////////////////////////
        private void CashInButton_Click(object sender, EventArgs e)
        {
            decimal cash;
            cash = decimal.Parse(CashInTextBox.Text);
            if (cash < 100m)
            {
                _wrongCash.ShowDialog();
                CashInTextBox.Clear();
            }
            else
            {
                _game.CashIn(cash);
                _game.DetermineStepsToWin(TrackBarRel.Value);
                RelPanel.Hide();
                _game.DetermineWinMethod();
                CashInPanel.Hide();
                CashInTextBox.Clear();
                BalanceAmount.Text = string.Format("${0}", _game.Balance.ToString());
            }
            ShowPanel(BetPanel);
        }

        private void Spin_Click(object sender, EventArgs e)
        {
            _game.Spin();
            SpinsAmount.Text = _game.Spins.ToString();
            SpinButton.Enabled = false;

            SlotValues Values = _game.DetermineSlotValues();

            SetDisplays(Values);
            SpinButton.Enabled = true;

            if (_game.Spins == 0 && _game.Won == false)
            {
                SpinButton.Enabled = false;
                ShowPanel(BetAgainPanel);
            }
            else if (_game.Won)
            {
                RelPanel.Show(); // Show The Relativity Panel
                _game.Win();
                SpinButton.Enabled = false;

                Values = _game.DetermineSlotValues();
                SetDisplays(Values);

                if (_game.WinCombination != WinCombinations.Six)
                {
                    AwardBalanceLabel.Text = String.Format("${0}", _game.Balance.ToString());
                    AwardPanel.Location = new Point(82, 330);
                    AwardPanel.Show();
                }
                else
                {
                    AwardBalanceLabel.Text = "+4 Sp";
                    SpinsAmount.Text = (Int32.Parse(SpinsAmount.Text) + 4).ToString();
                    ExitButton.Hide();
                    AwardPanel.Show();
                }
            }
        }

        private void DefaultHundred_Click(object sender, EventArgs e)
        {
            _game.DetermineStepsToWin(TrackBarRel.Value);
            _game.DetermineWinMethod();
            RelPanel.Hide();
            BalanceAmount.Text = String.Format("${0}", _game.Balance);
            SetBalancePanel.Hide();
            ShowPanel(BetPanel);
        }

        private void BetButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (BetTextBox.Text == ((decimal)Bets.Min).ToString() ||
                                BetTextBox.Text == ((decimal)Bets.Mid).ToString() ||
                                BetTextBox.Text == ((decimal)Bets.Max).ToString())
                {
                    _game.Bet((Bets)decimal.Parse(BetTextBox.Text));
                    BalanceAmount.Text = string.Format("${0}", _game.Balance.ToString());
                    CurrentBetAmount.Text = string.Format("${0}", BetTextBox.Text);
                    SpinsAmount.Text = _game.Spins.ToString();
                    BetTextBox.Clear();
                    BetPanel.Hide();
                    SpinButton.Enabled = true;
                }
                else
                {
                    _wrongBet.ShowDialog();
                    BetTextBox.Clear();
                }
            }
            catch (Exception)
            {
                NotEoughFundsLabel.Show();
                BetAgainBetButton.Enabled = false;
            }
        }

        private void BetAgainBetButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (BetAgainTextBox.Text == ((decimal)Bets.Min).ToString() || BetAgainTextBox.Text == ((decimal)Bets.Mid).ToString() || BetAgainTextBox.Text == ((decimal)Bets.Max).ToString())
                {
                    _game.Bet((Bets)decimal.Parse(BetAgainTextBox.Text));
                    BalanceAmount.Text = string.Format("${0}", _game.Balance.ToString());
                    CurrentBetAmount.Text = string.Format("${0}", BetAgainTextBox.Text);
                    SpinsAmount.Text = _game.Spins.ToString();
                    BetAgainTextBox.Clear();
                    BetAgainPanel.Hide();
                    SpinButton.Enabled = true;
                }
                else
                {
                    _wrongBet.ShowDialog();
                    BetAgainTextBox.Clear();
                }
            }
            catch (Exception)
            {
                NotEoughFundsLabel.Show();
                BetAgainBetButton.Enabled = false;
            }
        }

        private void AgainButton_Click(object sender, EventArgs e)
        {
            AwardPanel.Hide();
            BalanceAmount.Text = String.Format("${0}", _game.Balance.ToString());
            _game.DetermineStepsToWin(TrackBarRel.Value);
            RelPanel.Hide();
            _game.DetermineWinMethod();
            ShowPanel(BetAgainPanel);
        }

        private void RulesButton_Click(object sender, EventArgs e)
        {
            _rulePopup.ShowDialog();
        }

        private void CashIn_Click(object sender, EventArgs e)
        {
            SetBalancePanel.Hide();
            ShowPanel(CashInPanel);
        }
        ///////////////////////////////////////////////////////////////////////////
        #endregion


        // Exit
        public void CloseGame()
        {
            try
            {
                _rulePopup.Dispose();
                _wrongBet.Dispose();
                _wrongCash.Dispose();
            }
            finally
            {
                Environment.Exit(0);
            }
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            CloseGame();
        }
        private void LeaveRoom_Click(object sender, EventArgs e)
        {
            CloseGame();
        }
    }
}
