﻿using Gaminator.Enums;
using System;
using System.Windows.Forms;

namespace Gaminator
{
    public partial class WrongBet : Form
    {
        public WrongBet()
        {
            InitializeComponent();
        }

        private void WrongBet_Load(object sender, EventArgs e)
        {
            BetInfo.Text = string.Format("Min: {0}\n\nMid: {1}\n\nMax: {2}", (decimal)Bets.Min, (decimal)Bets.Mid, (decimal)Bets.Max);
        }

        private void RulesCloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
